<?php

class ModelExtensionCustomizedFieldsCustomizedFields extends Model {

	public function createSchema() {
		
		$this->db->query('SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";');

		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customized_field` (
				`custom_field_id` int(11) NOT NULL, 
				`type` varchar(32) CHARACTER SET utf8 NOT NULL, 
				`status` tinyint(1) NOT NULL, 
				`sort_order` int(3) NOT NULL
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;"
		);

		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customized_field_description` (
			`custom_field_id` int(11) NOT NULL, 
			`language_id` int(11) NOT NULL, 
			`name` varchar(128) CHARACTER SET utf8 NOT NULL, 
			`value` text CHARACTER SET utf8 NOT NULL, 
			`hash` varchar(32) CHARACTER SET utf8 NOT NULL 
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;"
		);

		$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "customized_field` 
			ADD PRIMARY KEY (`custom_field_id`);"
		);
		
		$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "customized_field_description` 
			ADD PRIMARY KEY (`custom_field_id`,`language_id`), 
			ADD KEY `hashindex` (`hash`);"
		);
		
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "customized_field` 
			MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;"
		);
		
	}

	public function deleteSchema() {
		$this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "customized_field");
		$this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "customized_field_description");
	}

	public function addCustomField($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "customized_field` 
			SET type = '" . $this->db->escape($data['type']) . 
				"', status = '" . (int) $data['status'] . 
				"', sort_order = '" . (int) $data['sort_order'] . 
				"'"
		);

		$custom_field_id = $this->db->getLastId();

		foreach ($data['custom_field_description'] as $language_id => $value) {
			$this->db->query(
					"INSERT INTO " . DB_PREFIX . "customized_field_description 
						SET custom_field_id = '" . (int) $custom_field_id . 
					"', language_id = '" . (int) $language_id . 
					"', name = '" . $this->db->escape($value['name']) . 
					"', value = '" . $this->db->escape($value['value']) . 
					"', hash = '" . $this->db->escape($value['hash']) . 
					"'"
			);
		}

		return $custom_field_id;
	}

	public function editCustomField($custom_field_id, $data) {
		$this->db->query(
				"UPDATE `" . DB_PREFIX . "customized_field` 
			SET type = '" . $this->db->escape($data['type']) .
				"', status = '" . (int) $data['status'] .
				"', sort_order = '" . (int) $data['sort_order'] .
				"' WHERE custom_field_id = '" . (int) $custom_field_id . "'"
		);

		$this->db->query(
				"DELETE FROM " . DB_PREFIX . "customized_field_description 
					WHERE custom_field_id = '" . (int) $custom_field_id . "'"
		);

		foreach ($data['custom_field_description'] as $language_id => $value) {
			$this->db->query(
					"INSERT INTO " . DB_PREFIX . "customized_field_description 
						SET custom_field_id = '" . (int) $custom_field_id .
					"', language_id = '" . (int) $language_id .
					"', name = '" . $this->db->escape($value['name']) . 
					"', value = '" . $this->db->escape($value['value']) . 
					"', hash = '" . $this->db->escape($value['hash']) .
					"'"
			);
		}
	}

	public function deleteCustomField($custom_field_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customized_field` WHERE custom_field_id = '" . (int) $custom_field_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customized_field_description` WHERE custom_field_id = '" . (int) $custom_field_id . "'");
	}

	public function getCustomField($custom_field_id) {
		$query = $this->db->query(
				"SELECT * FROM `" . DB_PREFIX . "customized_field` cf 
					LEFT JOIN " . DB_PREFIX . "customized_field_description cfd ON (cf.custom_field_id = cfd.custom_field_id) 
					WHERE cf.custom_field_id = '" . (int) $custom_field_id .
				"' AND cfd.language_id = '" . (int) $this->config->get('config_language_id') . "'"
		);

		return $query->row;
	}

	public function getCustomFields($data = array()) {

		$sql = "SELECT * FROM `" . DB_PREFIX . "customized_field` cf 
				LEFT JOIN " . DB_PREFIX . "customized_field_description cfd ON (cf.custom_field_id = cfd.custom_field_id) 
					WHERE cfd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cfd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'cf.custom_field_id',
			'cfd.name',
			'cf.type',
			'cf.status',
			'cf.sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY cfd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCustomFieldDescriptions($custom_field_id) {
		$custom_field_data = array();

		$query = $this->db->query(
				"SELECT * FROM " . DB_PREFIX . "customized_field_description 
					WHERE custom_field_id = '" . (int) $custom_field_id . "'"
		);

		foreach ($query->rows as $result) {
			$custom_field_data[$result['language_id']] = array(
				'name' => $result['name'], 
				'value' => $result['value']
					);
		}

		return $custom_field_data;
	}

	public function getTotalCustomFields() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "customized_field`");

		return $query->row['total'];
	}

}
