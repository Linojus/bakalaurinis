<?php
// Heading
$_['heading_title']    = 'Customized Fields';

// Text
$_['text_extension']   = 'Extensions';

$_['text_success']				= 'Success: You have modified custom fields!';
$_['text_list']					= 'Customized Field List';
$_['text_add']					= 'Add Customized Field';
$_['text_edit']					= 'Edit Customized Field';
$_['text_input']				= 'Input';
$_['text_textarea']				= 'Textarea';
$_['text_file']					= 'File';
$_['text_image']				= 'Image';
$_['text_customized_field']		= 'Customized Field';

$_['text_upload']				= 'Your file was successfully uploaded!';


// Column
$_['column_id']				= 'Id';
$_['column_name']			= 'Customized Field Name';
$_['column_type']			= 'Type';
$_['column_sort_order']		= 'Sort Order';
$_['column_action']			= 'Action';

// Entry
$_['entry_name']			= 'Customized Field Name';
$_['entry_type']			= 'Type';
$_['entry_value']			= 'Value';
$_['entry_required']		= 'Required';
$_['entry_status']			= 'Status';
$_['entry_sort_order']		= 'Sort Order';

// Help
$_['help_sort_order']		= 'Use minus to count backwards from the last field in the set.';

// Error
$_['error_permission']		= 'Warning: You do not have permission to modify Customized Fields module!';
$_['error_name']			= 'Custom Field Name must be between 1 and 128 characters!';

$_['error_upload']			= 'Upload required!';
$_['error_filename']		= 'Filename must be between 3 and 128 characters!';
$_['error_exists']			= 'File does not exist!';
$_['error_filetype']		= 'Invalid file type!';
