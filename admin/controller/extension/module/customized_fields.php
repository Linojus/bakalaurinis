<?php

class ControllerExtensionModuleCustomizedFields extends Controller {

	private $error = array();

	public function index() {
		$this->load->language('extension/module/customized_fields');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/customized_fields/customized_fields');

		$this->getList();
	}

	public function install() {
		$this->load->model('extension/customized_fields/customized_fields');
		$this->model_extension_customized_fields_customized_fields->createSchema();

		$this->load->model('setting/setting');
		$this->model_setting_setting->editSetting('module_customized_fields', array('module_customized_fields_status' => 1));
	}

	public function uninstall() {
		$this->load->model('extension/customized_fields/customized_fields');
		$this->model_extension_customized_fields_customized_fields->deleteSchema();
	}

	public function getValue($data, $type) {
		$value = "";

		switch ($type) {
			case 'input':
				$value = $data['value-input'];
				break;
			case 'textarea':
				$value = $data['value-textarea'];
				break;
			case 'file':
				$value = $data['value-file'];
				break;
			case 'image':
				$value = $data['value-image'];
				break;
		}
		return $value;
	}

	public function getValueByType($data) {
		$value = "";

		switch ($data['type']) {
			case 'input':
				$value = $data['value-input'];
				break;
			case 'textarea':
				$value = $data['value-textarea'];
				break;
			case 'file':
				$value = $data['value-file'];
				break;
			case 'image':
				$value = $data['value-image'];
				break;
		}
		return $value;
	}

	public function hashFilename($filename) {

		$hashed_filename = password_hash(trim($filename), PASSWORD_DEFAULT);

		$fn = str_replace(array(",", ".", "/"), "", $hashed_filename);

		return substr($fn, -32);
	}
	
	public function add() {
		$this->load->language('extension/module/customized_fields');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/customized_fields/customized_fields');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$customized_field_data = $this->request->post;

			foreach ($customized_field_data['custom_field_description'] as $language_id => $value) {
				$customized_field_data['custom_field_description'][$language_id]['value'] = $this->getValue($customized_field_data['custom_field_description'][$language_id], $customized_field_data['type']);
				
				// hash the filename
				if ($customized_field_data['type'] == "file") {
					$customized_field_data['custom_field_description'][$language_id]['hash'] = $this->hashFilename($customized_field_data['custom_field_description'][$language_id]['value']);
				} else {
					$customized_field_data['custom_field_description'][$language_id]['hash'] = "";
				}
			}

			$this->model_extension_customized_fields_customized_fields->addCustomField($customized_field_data);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}


	public function edit() {
		$this->load->language('extension/module/customized_fields');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/customized_fields/customized_fields');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$customized_field_data = $this->request->post;

			foreach ($customized_field_data['custom_field_description'] as $language_id => $value) {
				$customized_field_data['custom_field_description'][$language_id]['value'] = $this->getValue($customized_field_data['custom_field_description'][$language_id], $customized_field_data['type']);
				
				// hash the filename
				if ($customized_field_data['type'] == "file") {
					$customized_field_data['custom_field_description'][$language_id]['hash'] = $this->hashFilename($customized_field_data['custom_field_description'][$language_id]['value']);
				} else {
					$customized_field_data['custom_field_description'][$language_id]['hash'] = "";
				}
			}

			$this->model_extension_customized_fields_customized_fields->editCustomField($this->request->get['custom_field_id'], $customized_field_data);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('extension/module/customized_fields');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/customized_fields/customized_fields');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $custom_field_id) {
				$this->model_extension_customized_fields_customized_fields->deleteCustomField($custom_field_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'cfd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'], true)
		);


		$data['add'] = $this->url->link('extension/module/customized_fields/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('extension/module/customized_fields/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['custom_fields'] = array();

		$filter_data = array(
			'sort' => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);


		$custom_field_total = $this->model_extension_customized_fields_customized_fields->getTotalCustomFields();

		$results = $this->model_extension_customized_fields_customized_fields->getCustomFields($filter_data);

		foreach ($results as $result) {
			$type = '';

			switch ($result['type']) {
				case 'input':
					$type = $this->language->get('text_input');
					break;
				case 'textarea':
					$type = $this->language->get('text_textarea');
					break;
				case 'file':
					$type = $this->language->get('text_file');
					break;
				case 'image':
					$type = $this->language->get('text_image');
					break;
			}

			$data['custom_fields'][] = array(
				'custom_field_id' => $result['custom_field_id'],
				'name' => $result['name'],
				'type' => $type,
				'status' => $result['status'],
				'sort_order' => $result['sort_order'],
				'edit' => $this->url->link('extension/module/customized_fields/edit', 'user_token=' . $this->session->data['user_token'] . '&custom_field_id=' . $result['custom_field_id'] . $url, true)
			);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array) $this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_id'] = $this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . '&sort=cf.custom_field_id' . $url, true);
		$data['sort_name'] = $this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . '&sort=cfd.name' . $url, true);
		$data['sort_type'] = $this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . '&sort=cf.type' . $url, true);
		$data['sort_status'] = $this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . '&sort=cf.status' . $url, true);
		$data['sort_sort_order'] = $this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . '&sort=cf.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $custom_field_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($custom_field_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($custom_field_total - $this->config->get('config_limit_admin'))) ? $custom_field_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $custom_field_total, ceil($custom_field_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/customized_fields_list', $data));
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['custom_field_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		// errors
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		$url = '';

		// sorts
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		//breadcrumbs
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'], true)
		);

		// actions
		if (!isset($this->request->get['custom_field_id'])) {
			$data['action'] = $this->url->link('extension/module/customized_fields/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('extension/module/customized_fields/edit', 'user_token=' . $this->session->data['user_token'] . '&custom_field_id=' . $this->request->get['custom_field_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('extension/module/customized_fields', 'user_token=' . $this->session->data['user_token'] . $url, true);

		//fetch data
		//  oc_custom_field + oc_custom_field_description
		if (isset($this->request->get['custom_field_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$custom_field_info = $this->model_extension_customized_fields_customized_fields->getCustomField($this->request->get['custom_field_id']);

			if ($custom_field_info) {
				$data['custom_field_id'] = $custom_field_info['custom_field_id'];
			}
		}

		$data['user_token'] = $this->session->data['user_token'];

		// language stuff
		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['custom_field_description'])) {
			$data['custom_field_description'] = $this->request->post['custom_field_description'];
		} elseif (isset($this->request->get['custom_field_id'])) {
			$custom_field_descriptions = $data['custom_field_description'] = $this->model_extension_customized_fields_customized_fields->getCustomFieldDescriptions($this->request->get['custom_field_id']);
		} else {
			$data['custom_field_description'] = array();
		}


		if (isset($this->request->post['type'])) {
			$data['type'] = $this->request->post['type'];
		} elseif (!empty($custom_field_info)) {
			$data['type'] = $custom_field_info['type'];
		} else {
			$data['type'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($custom_field_info)) {
			$data['status'] = $custom_field_info['status'];
		} else {
			$data['status'] = '';
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($custom_field_info)) {
			$data['sort_order'] = $custom_field_info['sort_order'];
		} else {
			$data['sort_order'] = '';
		}

		//autofill if error
		foreach ($data['custom_field_description'] as $language_id => $value) {

			$data['custom_field_description'][$language_id]['value_input'] 
					= $data['custom_field_description'][$language_id]['value_textarea'] 
					= $data['custom_field_description'][$language_id]['value_file'] 
					/*= $data['custom_field_description'][$language_id]['value_image'] */
					= "";
			
			if (isset($this->request->post['custom_field_description'][$language_id]['value-input']) 
					|| isset($this->request->post['custom_field_description'][$language_id]['value-textarea']) 
					|| isset($this->request->post['custom_field_description'][$language_id]['value-image']) 
					|| isset($this->request->post['custom_field_description'][$language_id]['value-file'])) {

				$data['custom_field_description'][$language_id]['value_' . $data['type']] = $this->getValue($this->request->post['custom_field_description'][$language_id], $data['type']);
			} elseif (!empty($custom_field_descriptions)) {
				$data['custom_field_description'][$language_id]['value_' . $data['type']] = $custom_field_descriptions[$language_id]['value'];
			} else {
				$data['custom_field_description'][$language_id]['value_' . $data['type']] = '';
			}
		}
		
		//images
		$this->load->model('tool/image');
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		foreach ($data['custom_field_description'] as $language_id => $value) {

			if ($data['type'] == 'image' 
					&& is_file(DIR_IMAGE . $data['custom_field_description'][$language_id]['value_image']) 
					&& strlen($data['custom_field_description'][$language_id]['value_image']) > 3) {
				$data['custom_field_description'][$language_id]['thumb'] = $this->model_tool_image->resize(
						$data['custom_field_description'][$language_id]['value_image'], 100, 100
				);
			} else {
				$data['custom_field_description'][$language_id]['thumb'] = $data['placeholder'];
			}
		}

		//---

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/customized_fields_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'extension/module/customized_fields')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['custom_field_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 128)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'extension/module/customized_fields')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function upload() {
		$this->load->language('catalog/download');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/download')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename . '.' . token(32);

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

			$json['filename'] = $file;

			$json['success'] = $this->language->get('text_upload');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}
