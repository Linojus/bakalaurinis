<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */


if (is_file('../../../../config.php')) {
	require_once('../../../../config.php');
}

if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
	$server = HTTPS_SERVER;
} else {
	$server = HTTP_SERVER;
}

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');

$user_id = $_POST['usrid'] . '/';
$rand = $_POST['hash'] . '/';

$options = array(
	'image_versions' => array(
                'thumbnail' => array(
                    'max_width' => 150,
                    'max_height' => 150
                )
            ),
	'upload_dir' => DIR_IMAGE . 'acc_consultation/' . $user_id . $rand,
    'upload_url' => $server . 'image/acc_consultation/' . $user_id . $rand,
    'delete_type' => 'POST',
	'accept_file_types' => '/\.(gif|jpe?g|png)$/i'
	);

$upload_handler = new UploadHandler($options);
