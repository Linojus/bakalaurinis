<?php

if (is_file('../../../../config.php')) {
	require_once('../../../../config.php');
}

if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
	$server = HTTPS_SERVER;
} else {
	$server = HTTP_SERVER;
}

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');

$user_id = $_POST['usrid'] . '/';
$rand = $_POST['hash'] . '/';

$options = array(
	'image_versions' => array(
                'thumbnail' => array(
                    'max_width' => 150,
                    'max_height' => 150
                )
            ),
	'upload_dir' => DIR_IMAGE . 'useruploads/profiles/' . $user_id . $rand,
    'upload_url' => $server . 'image/useruploads/profiles/' . $user_id . $rand,
    'delete_type' => 'POST'
	);

$upload_handler = new UploadHandler($options);
