<?php

class ModelExtensionModuleCustomizedFields extends Model {

	public function getCustomField($custom_field_id) {
		$query = $this->db->query(
				"SELECT * FROM `" . DB_PREFIX . "customized_field` cf LEFT JOIN `" . DB_PREFIX . "customized_field_description` cfd ON (cf.custom_field_id = cfd.custom_field_id) 
					WHERE cf.status = '1' 
					AND cf.custom_field_id = '" . (int) $custom_field_id . "' 
						AND cfd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getCustomFieldDownload($hash) {
				
		$query = $this->db->query(
				"SELECT * FROM `" . DB_PREFIX . "customized_field` cf LEFT JOIN `" . DB_PREFIX . "customized_field_description` cfd ON (cf.custom_field_id = cfd.custom_field_id) 
					WHERE cf.status = '1' 
					AND cfd.hash = '" . $this->db->escape($hash) . "' 
						AND cfd.language_id = '" . (int) $this->config->get('config_language_id') . "'");
		
		return $query->row;
	}

}
