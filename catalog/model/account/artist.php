<?php

class ModelAccountArtist extends Model {

    //==========================================================================ARTIST PROFILE
    // <editor-fold defaultstate="collapsed" desc="ARTIST PROFILE">

    /**
     * Gets artist's profile description
     * 
     * @param type $artist_id   ID of the artist, whose description to fetch
     * @return type             array of artist's description
     */
    public function getProfileDescription($artist_id) {

        $query = $this->db->query(
                "SELECT * FROM " . DB_PREFIX . "profile_description 
                    WHERE customer_id = '" . (int) $artist_id . "'");

        return $query->row;
    }
    
    /**
     * Creates or edits artists profile description
     * 
     * @param type $artist_id   ID of the artist, whose profile desription to edit
     * @param type $data        array of data to insert/update
     * @return type             returns query result
     */
    public function editProfileDescription($artist_id, $data){
        
        $query = $this->db->query(
        "INSERT INTO " . DB_PREFIX . "profile_description SET 
                customer_id = '" . (int)$artist_id . "', 
                description = '" . $this->db->escape((string)$data['profile_description']['description']) . "', 
                contacts = '" . $this->db->escape((string)$data['profile_description']['contacts']) . "' 
                    ON DUPLICATE KEY UPDATE 
                        description = '" . $this->db->escape((string)$data['profile_description']['description']) . "', 
                        contacts = '" . $this->db->escape((string)$data['profile_description']['contacts']) . "'"
        );
        
        return $query->rows;
        
    }

    public function getCustomerSettings($customer_id) {
        $query = $this->db->query(
                "SELECT * FROM " . DB_PREFIX . "customer_settings 
                    WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row;
    }
    
    public function editCustomerSettings($customer_id, $data) {
        $query = $this->db->query(
        "INSERT INTO " . DB_PREFIX . "customer_settings SET 
                customer_id = '" . (int)$customer_id . "', 
                show_email = '" . (bool)$data['customer_settings']['show_email'] . "', 
                show_phone = '" . (bool)$data['customer_settings']['show_phone'] . "' 
                    ON DUPLICATE KEY UPDATE 
                        show_email = '" . (bool)$data['customer_settings']['show_email'] . "', 
                        show_phone = '" . (bool)$data['customer_settings']['show_phone'] . "'"
        );
        
        return $query->rows;
    }
    
    // </editor-fold>
    
    //==========================================================================ARTIST INFO
    // <editor-fold defaultstate="collapsed" desc="ARTIST INFORMATION">
    /**
     * Gets main artist information
     * 
     * @param type $artist_id   ID of the artist
     * @return type             array artist information
     */
    public function getArtist($artist_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int) $artist_id . "'");

        return $query->row;
    }    
    
    /**
     * Gets public artist's information
     * 
     * @param type $artist_id   ID of the artist
     * @return boolean          false, if info is not found
     */
    public function getArtistPublicInfo($artist_id) {
        
        $query = $this->db->query(""
                . "SELECT c.*, pd.description AS description, pd.contacts AS contacts, cs.*
                    FROM " . DB_PREFIX . "customer c 
                    LEFT JOIN " . DB_PREFIX . "profile_description pd
                    ON pd.customer_id = c.customer_id 
                    LEFT JOIN " . DB_PREFIX . "customer_settings cs 
                        ON cs.customer_id = c.customer_id
                        WHERE c.customer_id = '" . (int)$artist_id . "'"
        );

        if ($query->num_rows) {
            return array(
                'customer_id'           => $query->row['customer_id'],
                'customer_group_id'     => $query->row['customer_group_id'],
                'firstname'             => $query->row['firstname'],
                'lastname'              => $query->row['lastname'],
                'email'                 => $query->row['email'],
                'telephone'             => $query->row['telephone'],
                'status'                => $query->row['status'],
                'date_added'            => $query->row['date_added'],
                'description'           => $query->row['description'],
                'contacts'              => $query->row['contacts'],
                'show_email'            => $query->row['show_email'],
                'show_phone'            => $query->row['show_phone'],
                'image'                 => $query->row['image']
            );
        } else {
            return false;
        }
    }
    // </editor-fold>
    //==========================================================================ARTIST LINKS
    // <editor-fold defaultstate="collapsed" desc="ARTIST LINKS">
    
    /**
     * Gets artists profile links
     * 
     * @param type $artist_id   ID of artist whose links to fetch
     * @return type             array of links and their information
     */
    public function getArtistLinks($artist_id) {
        
        $query = $this->db->query(
                "SELECT pl.value, plt.*
                    FROM " . DB_PREFIX . "profile_link pl 
                    LEFT JOIN " . DB_PREFIX . "profile_link_type plt
                    ON pl.profile_link_type_id = plt.profile_link_type_id 
                        WHERE pl.customer_id = '" . (int)$artist_id . "' ORDER BY plt.sort_order ASC, plt.profile_link_type_id ASC"
        );

        return $query->rows;
    }
    
    /**
     * Gets profile link types + values of corresponding artist
     * 
     * @param type $artist_id   ID of artist, whose data to fetch
     * @return type             array of artists links data
     */
    public function getArtistProfileLinks($artist_id) {

        $query = $this->db->query(
                "SELECT pl.value, plt.*
                    FROM " . DB_PREFIX . "profile_link_type plt 
                    LEFT JOIN " . DB_PREFIX . "profile_link pl
                    ON pl.profile_link_type_id = plt.profile_link_type_id 
                        AND pl.customer_id = '" . (int)$artist_id . "' 
                            ORDER BY plt.sort_order ASC, plt.profile_link_type_id ASC"
        );
        return $query->rows;
    }
    
    /**
     * Edits artist's profile links
     * 
     * @param type $artist_id   ID of artist whose links to edit
     * @param type $data        array of links
     * @return type             returns result of query
     */
    public function editArtistProfileLinks($artist_id, $data) {

        foreach ($data['profile_links'] as $key => $value) {
            $query = $this->db->query(
            "INSERT INTO " . DB_PREFIX . "profile_link SET 
                    customer_id = '" . (int)$artist_id . "', 
                    profile_link_type_id = '" . (int) $key . "', 
                    value = '" . $this->db->escape((string)$value) . "' 
                        ON DUPLICATE KEY UPDATE 
                            value = '" . $this->db->escape((string)$value) . "'"
            );
        }

        return $query->rows;
    }

    // </editor-fold>
    //==========================================================================ARTIST FOLLOWERS
    // <editor-fold defaultstate="collapsed" desc="ARTIST FOLLOWERS">
    /**
     * Checks if a customer is following an artist
     * 
     * @param type $artist_id       ID of the artist
     * @param type $customer_id     ID of the customer
     * @return type                 results of the search
     */
    public function isFollowing($artist_id, $customer_id) {
        $query = $this->db->query(
                "SELECT * FROM " . DB_PREFIX . "followed_artist  
                    WHERE artist_id = '" . (int)$artist_id . "' 
                    AND customer_id = '" . (int)$customer_id . "' "
        );
        
        return $query->rows;
    }
    
    /**
     * Toggles artist following.
     * If the customer is already following the artist, customer unfollows artist;
     * else, customer follows the artist
     *
     * @param type $artist_id       ID of the artist
     * @param type $customer_id     ID of the customer
     * @param type $following       is the customer already following
     * @return type                 query results
     */
    public function toggleFollow($artist_id, $customer_id, $following) {
        if ($following) {
            $str = "DELETE FROM " . DB_PREFIX . "followed_artist 
                WHERE artist_id = '" . (int)$artist_id . "' 
                AND customer_id = '" . (int)$customer_id . "' ";
        } else {
            $str = "INSERT INTO " . DB_PREFIX . "followed_artist SET 
                artist_id = '" . (int)$artist_id . "', 
                customer_id = '" . (int)$customer_id . "' ";
        }
        
        $query = $this->db->query($str);
        
        return $query;
    }
    
    /**
     * Gets the followers of a specific artist
     * 
     * @param type $artist_id   ID of the artist, whose followers to fetch
     * @return type             array of followers
     */
    public function getFollowers ($artist_id) {
        $query = $this->db->query(
                "SELECT * FROM " . DB_PREFIX . "followed_artist  
                    WHERE artist_id = '" . (int)$artist_id . "'"
        );
        
        return $query->rows;
    }
    
    /**
     * Gets the count of followers of a specific artist
     * 
     * @param type $artist_id
     * @return type
     */
    public function getFollowerCount ($artist_id) {
        $query = $this->db->query(
                "SELECT COUNT(customer_id) AS follower_count FROM " . DB_PREFIX . "followed_artist  
                    WHERE artist_id = '" . (int)$artist_id . "'"
        );
        
        return $query->row['follower_count'];
    }
    
    /**
     * Gets the customers followed artists
     * 
     * @param type $customer_id     ID of the customer, whose followed artists to fetch
     * @return type
     */
    public function getFollowedArtists ($customer_id) {
        $query = $this->db->query(
                "SELECT * FROM " . DB_PREFIX . "followed_artist  
                    WHERE customer_id = '" . (int)$customer_id . "' "
        );
        
        return $query->rows;
    }
    // </editor-fold>
    
    //==========================================================================
}
