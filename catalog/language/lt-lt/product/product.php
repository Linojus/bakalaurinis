<?php
//
// developer.lt
//

$_['heading_title']          = 'Skelbimai';

// Text
$_['text_search']       = 'Paiška';
$_['text_brand']        = 'Gamintojas';
$_['text_manufacturer']   = 'Gamintojas:';
$_['text_model']          = 'Modelis:';
$_['text_reward']       = 'Lojalumo taškai:'; 
$_['text_points']       = 'Kaina lojalumo taškais:';
$_['text_stock']        = '';
$_['text_instock']        = 'Yra prekyboje';
$_['text_price']        = 'Kaina:'; 
$_['text_tax']          = 'Be PVM:'; 
$_['text_discount']     = '%s arba daugiau %s';
$_['text_option']       = 'Galimos modifikacijos';
$_['text_qty']          = 'Kiekis:';
$_['text_minimum']      = 'Šios prekės minimalus užsakymas yra %s';
$_['text_or']           = '- ARBA -';
$_['text_reviews']      = '%s vertinimai'; 
$_['text_write']        = 'Įvertinkite prekę';
$_['text_no_reviews']   = 'Šiai prekei nėra vertinimų.';
$_['text_on']           = ' ';
$_['text_note']         = '<span style="color: #FF0000;">Pastaba:</span> HTML yra nepalaikomas!';
$_['text_share']        = 'Dalintis';
$_['text_success']      = 'Ačiū už paliktą vertinimą. Jis buvo nusiųstas tvirtinimui.';
$_['text_upload']       = 'Jūsų rinkmena sėkmingai įkelta!';
$_['text_wait']         = 'Prašome palaukti!';
$_['text_tags']         = 'Žymės:';
$_['text_error']        = 'Prekė nerasta!';
$_['text_make_offer_title'] = 'Kainos siūlymas';
$_['text_make_offer']	= 'Netenkina mūsų kaina? <a id="make_offer_link">Pasiūlykite savo</a>!';

$_['text_no_results']   = 'Skelbimų, atitinkačių paieškos kriterijus, nėra.';

$_['text_add']	= 'Naujas skelbimas';
$_['text_edit']	= 'Skelbimo redagavimas';

$_['text_image']                = 'Nuotrauka';
$_['text_image_additional']     = 'Papildomos nuotraukos';

$_['text_artist']               = 'Menininkas';
$_['text_location']             = 'Vieta';
$_['text_weight']               = 'Svoris';
$_['text_dimensions']           = 'Išmatavimai (I x P x A)';
$_['text_date_added']           = 'Patalpinta';
$_['text_date_modified']        = 'Paskutinį kartą redaguota';




// Entry
$_['entry_name']          = 'Jūsų vardas:';
$_['entry_review']        = 'Jūsų vertinimas:';
$_['entry_rating']        = 'Vertinimas:';
$_['entry_good']          = 'Geras';
$_['entry_bad']           = 'Blogas';
$_['entry_captcha']       = 'Į žemiau esantį laukelį įveskite patikros kodą:';
$_['entry_size']		  = 'Išmiera';

$_['entry_description']             = 'Aprašymas';
$_['entry_tag']                     = 'Skelbimo žymės';
$_['entry_category']                = 'Kategorijos';
$_['entry_filter']                  = 'Filtrai';
$_['entry_location']                = 'Vietovė';
$_['entry_price']                   = 'Kaina';
$_['entry_dimension']               = 'Dimensijos (Ilg. x Pl. x Aukšt.)';
$_['entry_length']                  = 'Ilgis';
$_['entry_width']                   = 'Plotis';
$_['entry_height']                  = 'Aukštis';
$_['entry_length_class']            = 'Ilgio klasė';
$_['entry_weight']                  = 'Svoris';
$_['entry_weight_class']            = 'Svorio klasė';
$_['entry_image']                   = 'Nuotrauka';
$_['entry_sort_order']              = 'Rūšiavimo tvarka';

$_['entry_product_name']            = 'Pavadinimas';
$_['entry_advertisement_status']    = 'Pardavimo būsena';




//help
$_['help_tag']		= 'Atskirtos kableliu';
$_['help_category']	= '(Autoužbaigimas)';
$_['help_filter']	= '(Autoužbaigimas)';


// Tabs
$_['tab_description']       = 'Aprašymas';
$_['tab_attribute']         = 'Specifikacija';
$_['tab_review']            = 'Įvertinimai (%s)';
$_['tab_related']           = 'Panašios prekės'; 

// Error
$_['error_name']            = 'Klaida: Atsiliepimo autoriaus vardas turi turėti nuo 3 iki 25 simbolių!';
$_['error_text']            = 'Klaida: Atsiliepimas turi turėti nuo 25 iki 1000 simbolių!';
$_['error_rating']          = 'Klaida: Prašome įvertinkite prekę!';
$_['error_captcha']         = 'Klaida: Patikros kodas nesutampa su esančiu paveikslėlyje!';
$_['error_upload']          = 'Įkelimas yra privalomas!';
$_['error_filename']        = 'Rinkmenos pavadinimas turi būti nuo 3 iki 64 simbolių!';
$_['error_filetype']        = 'Netinkamas rinkmenos formatas!';
$_['error_required']        = '%s privalomas!';

$_['error_warning']         = 'Klaida: Prašome peržiūrėti formą dėl klaidų!';
$_['error_permission']      = 'Klaida: Neturite teisių redaguoti skelbimo!';

?>
