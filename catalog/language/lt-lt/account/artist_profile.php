<?php

$_['heading_title']                         = 'Menininko profilis';

$_['text_rankings']                         = 'Reitingai';
$_['text_followers']                        = 'Sekėjai';
$_['text_about_artist']                     = 'Apie menininką';

$_['text_follow']                           = 'sekti';
$_['text_unfollow']                         = 'nebesekti';

$_['text_artist_links']                     = "Menininko nuorodos";

$_['entry_artist_id']                           = 'Menininko ID';
$_['entry_artist_name']                         = 'Vardas';
$_['entry_artist_email']                        = 'El. Paštas';
$_['entry_artist_phone']                        = 'Tel. nr.';
$_['entry_artist_date_added']                   = 'Užsiregistravo';
$_['entry_artist_description']                  = "Menininko aprašymas";
$_['entry_artist_contacts']                     = 'Kaip su manimi susisiekti?';

