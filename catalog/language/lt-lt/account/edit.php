<?php
//
// developer.lt
//

// Heading 
$_['heading_title']       = 'Mano paskyros informacija';

// Text
$_['text_account']        = 'Paskyra';
$_['text_edit']           = 'Redaguoti informaciją';
$_['text_your_details']   = 'Jūsų asmeniniai duomenys';
$_['text_success']        = 'Jūsų paskyra sėkmingai atnaujinta.';

// Entry
$_['entry_firstname']     = 'Vardas:';
$_['entry_lastname']      = 'Pavardė:';
$_['entry_email']         = 'El. pašto adresas:';
$_['entry_telephone']     = 'Telefonas:';
$_['entry_fax']           = 'Faksas:';

// Error
$_['error_exists']        = 'Nurodytas el. pa6to adresas au užregistruotas!';
$_['error_firstname']     = 'Vardas turi turėti nuo 1 iki 32 simbolių!';
$_['error_lastname']      = 'Pavardė turi turėti nuo 1 iki 32 simbolių!';
$_['error_email']         = 'El. pašto adresas įvestas klaidingai!';
$_['error_telephone']     = 'Telefono numeris turi turėti nuo 3 iki 32 simbolių!';


//=============================================================================

// Text

$_['text_profile_description']  = 'Apie mane';
$_['text_your_profile_links']   = 'Mano nuorodos';
$_['text_customer_settings']    = 'Nnustatymai ';

$_['text_profile_image']        = 'Profilio nuotrauka';
$_['text_image_max_count']      = 'Maksimalus nuotraukų skaičius';
$_['text_image_max_size']       = 'Maksimalus nuotraukos dydis';
$_['text_add_image']            = 'Pridėti nuotrauką';


$_['entry_profile_description_description']     = 'Aprašymas';
$_['entry_profile_description_contacts']        = 'Mano kontaktai';

$_['entry_settings_show_email']    = 'Rodyti mano el. paštą';
$_['entry_settings_show_phone']    = 'Rodyti mano telefono nr.';



// Error
$_['error_custom_field'] = '%s reikalingi!';

$_['error_profile_description_description'] = 'Aprašymas per ilgas!';
$_['error_profile_description_contacts']    = 'Kontaktinė informacija per ilga!';
$_['error_profile_link']                    = 'Nuoroda per ilga!';
$_['error_image_count']                     = 'Per daug įkelta nuotraukų!';
$_['error_image_general']                   = 'Nepavyko įkelti nuotraukos!';


//info
$_['info_profile_description_description']     = 'Jūsų biografija arba tiesiog informacija apie jus ir ką jūs veikiate';
$_['info_profile_description_contacts']        = 'Kaip žmonės gali susisiekti su jumis?';
