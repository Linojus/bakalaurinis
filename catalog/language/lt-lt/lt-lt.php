<?php
//
// developer.lt
//

// Locale
$_['code']			= 'lt';
$_['direction']			= 'ltr';
$_['date_format_short']		= 'Y.m.d';
$_['date_format_long']		= 'l Y F dS';
$_['time_format']		= 'H:i:s ';
$_['decimal_point']		= '.';
$_['thousand_point']		= ' ';

// Text
$_['text_home']			= 'Pagrindinis';
$_['text_yes']			= 'Taip';
$_['text_no']			= 'Ne';
$_['text_none']			= ' --------- ';
$_['text_select']		= ' --- Pasirinkite --- ';
$_['text_all_zones']		= 'Visos zonos';
$_['text_pagination']		= 'Skelbimai nuo %d iki %d iš %d (%d puslapių (-io))';
$_['text_separator']		= ' &gt; ';

// Buttons
$_['button_add_address']	= 'Pridėti adresą';
$_['button_back']		    = 'Atgal';
$_['button_continue']       = 'Tęsti';
$_['button_cart']           = 'Į krepšelį';
$_['button_compare']        = 'Į palyginimą';
$_['button_wishlist']       = 'Į pageidavimus';
$_['button_checkout']       = 'Formuoti užsakymą';
$_['button_confirm']        = 'Patvirtinti užsakymą';
$_['button_coupon']         = 'Pritaikyti kuponą';
$_['button_delete']         = 'Šalinti';
$_['button_download']       = 'Parsisiųsti';
$_['button_edit']		= 'Redaguoti';
$_['button_filter']         = 'Filtruoti';
$_['button_delete']		= 'Šalinti';
$_['button_change_address'] = 'Keisti adresą';
$_['button_reviews']		= 'Komentarai';
$_['button_write']		= 'Rašyti komentarą';
$_['button_login']		= 'Prisijungti';
$_['button_update']		= 'Atnaujinti';
$_['button_remove']         = 'Šalinti';
$_['button_reorder']        = 'Rūšiuoti';
$_['button_return']         = 'Grįžti';
$_['button_shopping']		= 'Tęsti apsipirkimą';
$_['button_search']         = 'Ieškoti';
$_['button_shipping']       = 'Pritaikyti pristatymą';
$_['button_guest']          = 'Apsipirkiams be registracijos';
$_['button_view']		= 'Peržiūra';
$_['button_search']		= 'Paieška';
$_['button_go']			= 'Pirmyn';
$_['button_upload']         = 'Įkelti rinkmeną';
$_['button_coupon']		= 'Panaudoti kuponą';
$_['button_quote']          = 'Gauti keitimo kursus';

// Error
$_['error_upload_1']        = 'Dėmesio: įkeltos rinkmenos dydis viršyja parametro upload_max_filesize reikšmę, esančią php.ini!';
$_['error_upload_2']        = 'Dėmesio: įkeltos rinkmenos dydis viršyja parametro MAX_FILE_SIZE reikšmę, nurodytą HTML formoje!';
$_['error_upload_3']        = 'Dėmesio: rinkmena įkelta tik dalinai!';
$_['error_upload_4']        = 'Dėmesio: jokių rinkmenų neįkelta!';
$_['error_upload_6']        = 'Dėmesio: nerastas laikinasis katalogas!';
$_['error_upload_7']        = 'Dėmesio: nepavyko įrašyti rinkmenos į kaupiklį!';
$_['error_upload_8']        = 'Dėmesio: rinkmenos įkelimas sustabdytas pagal išplėtimą!';
$_['error_upload_999']      = 'Dėmesio: nenustatytas klaidos kodas!';
?>