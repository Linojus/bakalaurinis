<?php

$_['heading_title']                         = 'Artist profile';

$_['text_rankings']                         = 'Rankings';
$_['text_followers']                        = 'Followers';
$_['text_about_artist']                     = 'About artist';

$_['text_follow']                           = 'follow';
$_['text_unfollow']                         = 'unfollow';

$_['text_artist_links']                     = "Artist's links";

$_['entry_artist_id']                           = 'Artist ID';
$_['entry_artist_name']                         = 'Name';
$_['entry_artist_email']                        = 'Email';
$_['entry_artist_phone']                        = 'Phone';
$_['entry_artist_date_added']                   = 'Date joined';
$_['entry_artist_description']                  = "Artist's bio";
$_['entry_artist_contacts']                     = 'How to get in touch';

