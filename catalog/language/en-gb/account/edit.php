<?php
// Heading
$_['heading_title']      = 'My Account Information';

// Text
$_['text_account']       = 'Account';
$_['text_edit']          = 'Edit Information';
$_['text_your_details']  = 'Your Personal Details';
$_['text_success']       = 'Success: Your account has been successfully updated.';

$_['text_profile_description']  = 'About Me';
$_['text_your_profile_links']   = 'My Links';
$_['text_customer_settings']    = 'Settings ';

$_['text_profile_image']  = 'Profile image';
$_['text_image_max_count']      = 'Max number of images';
$_['text_image_max_size']       = 'Max image size';
$_['text_add_image']            = 'Add photo';


// Entry
$_['entry_firstname']    = 'First Name';
$_['entry_lastname']     = 'Last Name';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Telephone';

$_['entry_profile_description_description']     = 'Description';
$_['entry_profile_description_contacts']        = 'Contacts';

$_['entry_settings_show_email']    = 'Show my email';
$_['entry_settings_show_phone']    = 'Show my phone number';



// Error
$_['error_exists']       = 'Warning: E-Mail address is already registered!';
$_['error_firstname']    = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']     = 'Last Name must be between 1 and 32 characters!';
$_['error_email']        = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']    = 'Telephone must be between 3 and 32 characters!';
$_['error_custom_field'] = '%s required!';

$_['error_profile_description_description'] = 'Description is too long!';
$_['error_profile_description_contacts']    = 'Contacts are too long!';
$_['error_profile_link']                    = 'Link is too long!';
$_['error_image_count']                     = 'Too many images attached!';
$_['error_image_general']                   = 'Failed to upload image!';




//info
$_['info_profile_description_description']     = 'Your bio or just information about who you are and what you do';
$_['info_profile_description_contacts']        = 'How can people get in touch with you?';
