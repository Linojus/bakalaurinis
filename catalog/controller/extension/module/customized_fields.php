<?php

class ControllerExtensionModuleCustomizedFields extends Controller {

	private $error = array();

	public function getCustomField($cf_id) {

		if (isset($cf_id['id'])) {
			$custom_field_id = $cf_id['id'];

			// catalog/model/extension/module/
			$this->load->model('extension/module/customized_fields');

			$custom_field_info = $this->model_extension_module_customized_fields->getCustomField($custom_field_id);

			if ($custom_field_info) {

				$value = $this->getCustomFieldValue($custom_field_info);

				$results = array(
					'id' => $custom_field_info['custom_field_id'],
					'name' => htmlspecialchars_decode(stripslashes($custom_field_info['name'])),
					'type' => $custom_field_info['type'],
					'value' => $value['value']
				);

				if (isset($value['other'])) {
					if (isset($value['other']['href'])) {
						$results['href'] = $value['other']['href'];
					}
				}
				
				return $results;
				
			} else {
				return false;
			}
		}
		return false;
	}

	public function getCustomFieldValue($cf) {

		switch ($cf['type']) {
			case "input":
				$value = $this->getInputInfo($cf['value']);
				break;
			case "textarea":
				$value = $this->getTextareaInfo($cf['value']);
				break;
			case "image":
				$value = $this->getImageInfo($cf['value']);
				break;
			case "file":
				$value = $this->getFileInfo($cf);
				break;
		}
		return $value;
	}

	public function getInputInfo($value) {
		$val['value'] = htmlspecialchars_decode(stripslashes($value));
		$val['other'] = array();
		return $val;
	}

	public function getTextAreaInfo($value) {
		$val['value'] = htmlspecialchars_decode(stripslashes($value));
		$val['other'] = array();
		return $val;
	}

	public function getImageInfo($value) {
		$val['value'] = $value;

		//$image_new = str_replace(' ', '%20', $image_new);
		
		if ($this->request->server['HTTPS']) {
			$img_location = $this->config->get('config_ssl') . 'image/' . $value;
		} else {
			$img_location = $this->config->get('config_url') . 'image/' . $value;
		}

		$val['other'] = array(
			'href' => $img_location
		);

		return $val;
	}

	public function getFileInfo($value) {
		$val['value'] = $value['value'];
		$val['other'] = array(
			'href' => htmlspecialchars_decode(stripslashes($this->url->link('extension/module/customized_fields/download', 'file=' . $value['hash'], true)))
		);
		return $val;
	}

	public function download() {

		if (isset($this->request->get['file'])) {
			$hash = $this->request->get['file'];
		} else {
			$hash = 0;
		}

		$this->load->model('catalog/extension/module/customized_fields');
		$download_info = $this->model_catalog_extension_module_customized_fields->getCustomFieldDownload($hash);

		if ($download_info) {

			$file = DIR_DOWNLOAD . $download_info['value'];

			if (!headers_sent()) {
				if (file_exists($file)) {

					$filename = basename($file);

					if (substr_count($filename, ".") > 1) {
						$filename = substr($filename, 0, strrpos($filename, "."));
					}

					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename="' . $filename . '"');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($file));

					if (ob_get_level()) {
						ob_end_clean();
					}

					readfile($file, 'rb');

					exit();
				} else {
					exit('Error: Could not find file ' . $file . '!');
				}
			} else {
				exit('Error: Headers already sent out!');
			}
		} else {
			$this->response->redirect($this->url->link('common/home', '', true));
		}
	}

}
