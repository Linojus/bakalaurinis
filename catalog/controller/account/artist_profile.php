<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ControllerAccountArtistProfile extends Controller {

    
    private $thumb_width    = 200;
    private $thumb_height   = 200;
    /**
     * all profiles? OR view single with parameter
     */
    public function index() {
        
    }

    /**
     * view single profile
     */
    public function view() {
        $this->load->language('account/artist_profile');

        $this->document->setTitle($this->language->get('heading_title'));

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        
        /*
          $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
          $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
          $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
          $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
         */

        $this->load->model('account/artist');


        /*
          if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
          $this->model_account_customer->editCustomer($this->customer->getId(), $this->request->post);

          $this->session->data['success'] = $this->language->get('text_success');

          $this->response->redirect($this->url->link('account/account', 'language=' . $this->config->get('config_language')));
          }
         */



        $data['breadcrumbs'] = array();
        /*
          $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_home'),
          'href' => $this->url->link('common/home', 'language=' . $this->config->get('config_language'))
          );

          $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_account'),
          'href' => $this->url->link('account/account', 'language=' . $this->config->get('config_language'))
          );

          $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_edit'),
          'href' => $this->url->link('account/edit', 'language=' . $this->config->get('config_language'))
          );
         */


        /*
          if (isset($this->error['warning'])) {
          $data['error_warning'] = $this->error['warning'];
          } else {
          $data['error_warning'] = '';
          }

          if (isset($this->error['firstname'])) {
          $data['error_firstname'] = $this->error['firstname'];
          } else {
          $data['error_firstname'] = '';
          }

          if (isset($this->error['lastname'])) {
          $data['error_lastname'] = $this->error['lastname'];
          } else {
          $data['error_lastname'] = '';
          }

          if (isset($this->error['email'])) {
          $data['error_email'] = $this->error['email'];
          } else {
          $data['error_email'] = '';
          }

          if (isset($this->error['telephone'])) {
          $data['error_telephone'] = $this->error['telephone'];
          } else {
          $data['error_telephone'] = '';
          }

          if (isset($this->error['custom_field'])) {
          $data['error_custom_field'] = $this->error['custom_field'];
          } else {
          $data['error_custom_field'] = array();
          }

          $data['action'] = $this->url->link('account/edit', 'language=' . $this->config->get('config_language'));
         */

        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            if (isset($this->request->get['artist_id'])) {
                $artist_id = (int) $this->request->get['artist_id'];
            } else {
                $artist_id = 0;
            }
            $data['artist'] = $artist_info = $this->model_account_artist->getArtistPublicInfo($artist_id);

            $data['artist']['links'] = $artist_links = $this->model_account_artist->getArtistLinks($artist_id);
            $data['artist']['follower_count'] = $follower_count = $this->model_account_artist->getFollowerCount($artist_id);

            //profile image

            $this->load->model('tool/image');
            $data['height'] = $this->thumb_height;
            $data['width'] = $this->thumb_width;
            $image_data = $data['artist']['image'];
            if (strlen($image_data) > 3) {
                $img_name = basename($image_data);
                if (is_file(DIR_IMAGE . $image_data)) {
                    $thumb = $this->model_tool_image->resize($image_data, $data['width'], $data['height']);
                    $data['artist']['image'] = array(
                        'name' => $img_name,
                        'thumbnailUrl' => $thumb,
                        'url' => $server . 'image/' . dirname($image_data) . '/' . rawurlencode($img_name),
                        'path' => $image_data,
                        'uid' => uniqid()
                    );
                }
            }

            //if customer is logged in actions
            if ($this->customer->isLogged()) {

                $data['is_logged'] = TRUE;
                $data['is_customer'] = TRUE;
                $customer_id = $this->customer->getId();

                //check if logged in user is following this artist
                $result_is_following = $this->model_account_artist->isFollowing($artist_id, $customer_id);
                $data['is_following'] = count($result_is_following) > 0 ? TRUE : FALSE;

                $data['follow_action'] = $this->url->link('account/artist_profile/follow');

                /*
                  echo '<pre>' . var_export($data, TRUE) . '</pre>';
                  die;

                 */
            }
        }

        /*
          if (isset($this->request->post['firstname'])) {
          $data['firstname'] = $this->request->post['firstname'];
          } elseif (!empty($customer_info)) {
          $data['firstname'] = $customer_info['firstname'];
          } else {
          $data['firstname'] = '';
          }

          if (isset($this->request->post['lastname'])) {
          $data['lastname'] = $this->request->post['lastname'];
          } elseif (!empty($customer_info)) {
          $data['lastname'] = $customer_info['lastname'];
          } else {
          $data['lastname'] = '';
          }

          if (isset($this->request->post['email'])) {
          $data['email'] = $this->request->post['email'];
          } elseif (!empty($customer_info)) {
          $data['email'] = $customer_info['email'];
          } else {
          $data['email'] = '';
          }

          if (isset($this->request->post['telephone'])) {
          $data['telephone'] = $this->request->post['telephone'];
          } elseif (!empty($customer_info)) {
          $data['telephone'] = $customer_info['telephone'];
          } else {
          $data['telephone'] = '';
          }
         */



        // Custom Fields
        /*
          $data['custom_fields'] = array();

          $this->load->model('account/custom_field');

          $custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

          foreach ($custom_fields as $custom_field) {
          if ($custom_field['location'] == 'account') {
          $data['custom_fields'][] = $custom_field;
          }
          }

          if (isset($this->request->post['custom_field']['account'])) {
          $data['account_custom_field'] = $this->request->post['custom_field']['account'];
          } elseif (isset($customer_info)) {
          $data['account_custom_field'] = json_decode($customer_info['custom_field'], true);
          } else {
          $data['account_custom_field'] = array();
          }
         */

        $data['back'] = $this->url->link('account/account', 'language=' . $this->config->get('config_language'));

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('account/artist_profile_view', $data));
    }

    public function follow() {
        $json = array();

        if ($this->customer->isLogged()) {

            $this->load->model('account/artist');

            if ($this->request->server['REQUEST_METHOD'] == 'GET') {
                if (isset($this->request->get['artist_id'])) {
                    $artist_id = (int) $this->request->get['artist_id'];
                    $customer_id = $this->customer->getId();

                    //check if user is already following
                    $result_is_following = $this->model_account_artist->isFollowing($artist_id, $customer_id);
                    // if found rows > 0, then user is already following the artist
                    $following = count($result_is_following) > 0 ? TRUE : FALSE;

                    //toggle follow
                    $result = $this->model_account_artist->toggleFollow($artist_id, $customer_id, $following);

                    $json['data'] = $result;
                    $json['code'] = 200;
                    $json['message'] = "Success";
                } else {
                    $json['code'] = 400;
                    $json['message'] = 'Bad request';
                }
                /*
                  echo '<pre>' . var_export($artist_links, TRUE) . '</pre>';
                  die;
                 */
            } else {
                $json['code'] = 405;
                $json['message'] = 'Method not allowed';
            }
        } else {
            $json['code'] = 401;
            $json['message'] = 'Unauthorized';
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    
    /**
     * view all customer followed artists
     */
    public function following() {
        
    }
    
    
}
