<?php

class ControllerAccountEdit extends Controller {

    private $error = array();
    private $max_image_count = 1;
    private $max_image_size = 1000000;
    private $thumb_width = 250;
    private $thumb_height = 250;

    public function getHash() {
        return uniqid();
    }

    public function index() {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/edit', 'language=' . $this->config->get('config_language'));

            $this->response->redirect($this->url->link('account/login', 'language=' . $this->config->get('config_language')));
        }



        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        $data['customer_id'] = $customer_id = $this->customer->getId();


        $this->load->language('account/edit');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->load->model('account/customer');



        //check if current user is an artist
        $data['is_artist'] = $is_artist = $this->customer->isArtist();
        //load artist model
        $this->load->model('account/artist');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            /*
              echo '<pre>' . var_export($this->request->post, TRUE) . '</pre>';
              die;
             */
            if ($is_artist) {
                //update profile description
                $this->model_account_artist->editProfileDescription($this->customer->getId(), $this->request->post);
                //update profile links
                $this->model_account_artist->editArtistProfileLinks($this->customer->getId(), $this->request->post);
                //update customer settings
                $this->model_account_artist->editCustomerSettings($this->customer->getId(), $this->request->post);

                //add image
                if (isset($this->request->post['images']) && isset($this->request->post['images'][0]) && strlen($this->request->post['images'][0]) > 3) {
                    $img_url = 'useruploads/profiles/' 
                            . $customer_id . '/' 
                            . $this->request->post['hash'] . '/' 
                            . $this->request->post['images'][0];
                    $this->request->post['image'] = $img_url;
                }
            }

            $this->model_account_customer->editCustomer($this->customer->getId(), $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('account/account', 'language=' . $this->config->get('config_language')));
        }

        //breadcrumbs
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'language=' . $this->config->get('config_language'))
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', 'language=' . $this->config->get('config_language'))
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_edit'),
            'href' => $this->url->link('account/edit', 'language=' . $this->config->get('config_language'))
        );

        //errors
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['firstname'])) {
            $data['error_firstname'] = $this->error['firstname'];
        } else {
            $data['error_firstname'] = '';
        }

        if (isset($this->error['lastname'])) {
            $data['error_lastname'] = $this->error['lastname'];
        } else {
            $data['error_lastname'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }

        if (isset($this->error['custom_field'])) {
            $data['error_custom_field'] = $this->error['custom_field'];
        } else {
            $data['error_custom_field'] = array();
        }

        //LINJUO1 ERRORS
        if (isset($this->error['profile_description']['description'])) {
            $data['error_profile_description']['description'] = $this->error['profile_description']['description'];
        } else {
            $data['error_profile_description']['description'] = array();
        }

        if (isset($this->error['profile_description']['contacts'])) {
            $data['error_profile_description']['contacts'] = $this->error['profile_description']['contacts'];
        } else {
            $data['error_profile_description']['contacts'] = array();
        }

        if (isset($this->error['profile_links'])) {
            $data['error_profile_links'] = $this->error['profile_links'];
        } else {
            $data['error_profile_links'] = array();
        }

        if (isset($this->error['image'])) {
            $data['error_image'] = $this->error['image'];
        } else {
            $data['error_image'] = '';
        }

        //edit button
        $data['action'] = $this->url->link('account/edit', 'language=' . $this->config->get('config_language'));

        //FETCH INFO FOR EDIT
        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

            if ($is_artist) {
                //fetch description
                $profile_description = $this->model_account_artist->getProfileDescription($this->customer->getId());
                //fetch profile links
                $profile_links = $this->model_account_artist->getArtistProfileLinks($this->customer->getId());
                //fetch customer settings
                $customer_settings = $this->model_account_artist->getCustomerSettings($this->customer->getId());

            }
        }

        //LINJUO1 FILL OUT FORM DATA
        if ($is_artist) {
            //description
            if (isset($this->request->post['profile_description']['description'])) {
                $data['profile_description']['description'] = $this->request->post['profile_description']['description'];
            } elseif (!empty($profile_description)) {
                $data['profile_description']['description'] = $profile_description['description'];
            } else {
                $data['profile_description']['description'] = '';
            }

            if (isset($this->request->post['profile_description']['contacts'])) {
                $data['profile_description']['contacts'] = $this->request->post['profile_description']['contacts'];
            } elseif (!empty($profile_description)) {
                $data['profile_description']['contacts'] = $profile_description['contacts'];
            } else {
                $data['profile_description']['contacts'] = '';
            }

            //profile links
            if (isset($this->request->post['profile_links'])) {
                $pls = $this->model_account_artist->getArtistProfileLinks($this->customer->getId());
                foreach ($pls as $pl) {
                    foreach ($this->request->post['profile_links'] as $key => $value) {
                        if ($pl['profile_link_type_id'] == $key) {
                            $pl['value'] = $value;
                        }
                    }
                }
                $data['profile_links'] = $pls;
            } elseif (!empty($profile_links)) {
                $data['profile_links'] = $profile_links;
            } else {
                $data['profile_links'] = array();
            }

            //customer settings
            if (isset($this->request->post['customer_settings'])) {
                $data['customer_settings'] = $this->request->post['customer_settings'];
            } elseif (!empty($profile_description)) {
                $data['customer_settings'] = $customer_settings;
            } else {
                $data['customer_settings'] = array();
            }


            //PROFILE IMAGE

            if (isset($this->request->post['hash'])) {
                $data['hash'] = $this->request->post['hash'];
            } elseif (!empty($customer_info)) {
                if($customer_info['image']) {
                    $data['hash'] = basename(dirname($customer_info['image']));
                } else {
                    $data['hash'] = $this->getHash();
                }
            } else {
                $data['hash'] = $this->getHash();
            }

            // IMAGES
            $this->load->model('tool/image');
            $data['height'] = $this->thumb_height;
            $data['width'] = $this->thumb_width;
            if (isset($this->request->post['images'])) {
                $data['images'] = $this->request->post['images'];
            } elseif (!empty($customer_info)) {
                $data['images'] = array();
                if (isset($customer_info['image']) && strlen($customer_info['image']) > 3) {
                    $data['images'][] = array('image' => $customer_info['image']);
                } 
            } else {
                $data['images'] = array();
            }


            foreach ($data['images'] as $key => $value) {
                if (!isset($value['image'])) {
                    //$img_arr = explode("/", $value);
                    $value = array('image' => 'useruploads/profiles/' . $customer_id . '/' . $data['hash'] . '/' . $value);
                }
                $img_name = basename($value['image']);
                if (is_file(DIR_IMAGE . $value['image'])) {
                    $thumb = $this->model_tool_image->resize($value['image'], $data['width'], $data['height']);
                    $data['images'][$key] = array(
                        'name' => $img_name,
                        'thumbnailUrl' => $thumb,
                        'url' => $server . 'image/' . dirname($value['image']) . '/' . rawurlencode($img_name),
                        'delete_url' => $server . "image/jqfileupload/server/php/profile.php?file=" . rawurlencode($img_name) . "&amp;_method=DELETE",
                        'path' => $value['image'],
                        'uid' => uniqid()
                    );
                } else {
                    unset($data['images'][$key]);
                }
            }
            
  /*          
            echo '<pre>' . var_export($data, TRUE) . '</pre>';
            die;
*/
        }

        //FILL OUT FORM DATA
        if (isset($this->request->post['firstname'])) {
            $data['firstname'] = $this->request->post['firstname'];
        } elseif (!empty($customer_info)) {
            $data['firstname'] = $customer_info['firstname'];
        } else {
            $data['firstname'] = '';
        }

        if (isset($this->request->post['lastname'])) {
            $data['lastname'] = $this->request->post['lastname'];
        } elseif (!empty($customer_info)) {
            $data['lastname'] = $customer_info['lastname'];
        } else {
            $data['lastname'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } elseif (!empty($customer_info)) {
            $data['email'] = $customer_info['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $data['telephone'] = $this->request->post['telephone'];
        } elseif (!empty($customer_info)) {
            $data['telephone'] = $customer_info['telephone'];
        } else {
            $data['telephone'] = '';
        }

        // Custom Fields
        $data['custom_fields'] = array();
        $this->load->model('account/custom_field');
        $custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));
        foreach ($custom_fields as $custom_field) {
            if ($custom_field['location'] == 'account') {
                $data['custom_fields'][] = $custom_field;
            }
        }
        if (isset($this->request->post['custom_field']['account'])) {
            $data['account_custom_field'] = $this->request->post['custom_field']['account'];
        } elseif (isset($customer_info)) {
            $data['account_custom_field'] = json_decode($customer_info['custom_field'], true);
        } else {
            $data['account_custom_field'] = array();
        }


        $data['back'] = $this->url->link('account/account', 'language=' . $this->config->get('config_language'));

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $data['max_image_count'] = $this->max_image_count;
        $data['max_image_size'] = $this->max_image_size;
        $data['max_image_size_mb'] = ($this->max_image_size / 1000000) . "MB";
        $data['jqupload'] = $server . 'image/jqfileupload/';
        $data['server'] = $server;
        $data['customer_id'] = $customer_id;

        $this->response->setOutput($this->load->view('account/edit', $data));
    }

    protected function validate() {
        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
            $this->error['warning'] = $this->language->get('error_exists');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        // Custom field validation
        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

        foreach ($custom_fields as $custom_field) {
            if ($custom_field['location'] == 'account') {
                if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                } elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/' . html_entity_decode($custom_field['validation'], ENT_QUOTES, 'UTF-8') . '/')))) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                }
            }
        }

        //LINJUO1 VALIDATION
        $this->validateProfileDescription();
        $this->validateProfileLinks();
        $this->validateImages();

        return !$this->error;
    }

    protected function validateProfileDescription() {
        if ((utf8_strlen(trim($this->request->post['profile_description']['description'])) > 1000)) {
            $this->error['profile_description']['description'] = $this->language->get('error_profile_description_description');
        }

        if ((utf8_strlen(trim($this->request->post['profile_description']['contacts'])) > 500)) {
            $this->error['profile_description']['contacts'] = $this->language->get('error_profile_description_contacts');
        }
    }

    protected function validateProfileLinks() {

        foreach ($this->request->post['profile_links'] as $key => $value) {
            if ((utf8_strlen(trim($value)) > 50)) {
                $this->error['profile_links'][$key] = $this->language->get('error_profile_link');
            }
        }
    }

    protected function validateImages() {
        if (isset($this->request->post['images'])) {
            if (count($this->request->post['images']) > $this->max_image_count) {
                $this->error['image'] = $this->language->get('error_image_count');
            }
        }
    }

}
